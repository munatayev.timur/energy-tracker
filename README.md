# Energy tracker

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

[Official website](https://enelybot.com/en/)

Be aware of electricty costs, plan your activities and save the money.

## Description

The app helps you check electricity prices for the rest of the day.
See recommendations based on electricity prices for your region.
Save energy and money on everyday things like taking a shower, cooking food, doing laundry, vacuum cleaning, charging a car, or warming up your place.

### Bulet features

- fetching electricity prices by region
- advices for you activities

## How use
1. **Login**. Click start.
2. **Select region**. In a list select your country.
3. **Get prices**. Current electricity price will be shown with some hints. Additionally chart with todays prices alse available.
4. **Activities**. Select you favorite activities to get help when the best time to do them.

### What used

- MVI architecture
- Kotlin
- Kotlin Flow
- Kotlin coroutines
- Data binding
- Shared preferences
- State flow
- Hilt dependancy injection
- Retrofit REST API
- Unit tests

### Personal data

[Privacy Policy](https://pages.flycricket.io/energy-tracker-0/privacy.html "Privacy Policy")
[Terms &amp; Conditions](https://pages.flycricket.io/energy-tracker-0/terms.html "Terms &amp; Conditions")

## Download

<a href='https://play.google.com/store/apps/details?id=app.snappik.insureme&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' width="300" height="120" src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>


## Authors

* **Munatayev Timur**

## License

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0
