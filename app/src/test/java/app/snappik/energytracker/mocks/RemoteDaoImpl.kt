package app.snappik.energytracker.mocks

import app.snappik.energytracker.model.PricePair
import app.snappik.energytracker.model.dto.ActivityTimeDto
import app.snappik.energytracker.model.dto.PricesDto
import app.snappik.energytracker.model.dto.UserSignInDto
import app.snappik.energytracker.retrofit.RemoteDao
import java.lang.Exception

class RemoteDaoImpl : RemoteDao {

    private val prices = PricesDto(
        morning = listOf(
            PricePair(6, 1.0),
            PricePair(7, 2.0),
            PricePair(8, 3.0),
            PricePair(9, 4.0),
            PricePair(10, 5.0),
            PricePair(11, 6.0)),
        afternoon = listOf(
            PricePair(12, 1.0),
            PricePair(13, 2.0),
            PricePair(14, 3.0),
            PricePair(15, 4.0),
            PricePair(16, 5.0),
            PricePair(17, 6.0)),
        evening = listOf(
            PricePair(18, 1.0),
            PricePair(19, 2.0),
            PricePair(20, 3.0),
            PricePair(21, 4.0),
            PricePair(22, 5.0),
            PricePair(23, 6.0))
    )

    private val activities = ActivityTimeDto(
        morning = mapOf(1 to ActivityTimeDto.PartDayResult(1.0, 6),
            2 to ActivityTimeDto.PartDayResult(2.0, 7),
            3 to ActivityTimeDto.PartDayResult(3.0, 8),
            4 to ActivityTimeDto.PartDayResult(4.0, 9),
            5 to ActivityTimeDto.PartDayResult(5.0, 10),
            6 to ActivityTimeDto.PartDayResult(6.0, 11)),
        afternoon = mapOf(7 to ActivityTimeDto.PartDayResult(1.0, 12),
            8 to ActivityTimeDto.PartDayResult(2.0, 13),
            9 to ActivityTimeDto.PartDayResult(3.0, 14),
            10 to ActivityTimeDto.PartDayResult(4.0, 15),
            11 to ActivityTimeDto.PartDayResult(5.0, 16),
            12 to ActivityTimeDto.PartDayResult(6.0, 17)),
        evening = mapOf(13 to ActivityTimeDto.PartDayResult(1.0, 18),
            14 to ActivityTimeDto.PartDayResult(2.0, 19),
            15 to ActivityTimeDto.PartDayResult(3.0, 20),
            16 to ActivityTimeDto.PartDayResult(4.0, 21),
            17 to ActivityTimeDto.PartDayResult(5.0, 22),
            18 to ActivityTimeDto.PartDayResult(6.0, 23))
    )

    override suspend fun getPrices(which: String, `when`: String, messengerId: String): PricesDto {
        return if(pricesAvailable) prices else throw Exception()
    }

    override suspend fun getActivityTime(
        what: String,
        `when`: String,
        messengerId: String
    ): ActivityTimeDto {
       return if(activityTimeAvailable) activities else throw Exception()
    }

    override suspend fun createUser(userSignInDto: UserSignInDto) = Unit

    companion object {
        var activityTimeAvailable = true
        var pricesAvailable = true
    }
}