package app.snappik.energytracker.mocks

import android.content.Context
import app.snappik.energytracker.database.LocalDao
import app.snappik.energytracker.model.item.ActivityItem
import app.snappik.energytracker.model.item.RegionItem

class LocalDaoImpl : LocalDao {

    private var userId: String? = null
    private var region: RegionItem? = null
    private val activities = ArrayList<ActivityItem>()

    override val context: Context? = null

    override fun saveUserData(userIdValue: String, regionItem: RegionItem) {
        userId = userIdValue
        region = regionItem
    }

    override fun getUserId(): Pair<String, RegionItem>? {
        return userId?.let { uId ->
            region?.let { rId ->
                Pair(uId, rId)
            }
        }
    }

    override fun saveActivitiesData(list: List<ActivityItem>) {
        activities.clear()
        activities.addAll(list)
    }

    override fun getActivities(): List<ActivityItem>  = activities

    override fun clean(): Boolean {
        activities.clear()
        userId = null
        region = null
        return true
    }
}