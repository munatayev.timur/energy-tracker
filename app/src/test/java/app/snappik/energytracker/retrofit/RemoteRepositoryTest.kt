package app.snappik.energytracker.retrofit

import app.snappik.energytracker.mocks.RemoteDaoImpl
import app.snappik.energytracker.model.PricePair
import app.snappik.energytracker.model.item.ActivityItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class RemoteRepositoryTest {

    private lateinit var remoteRepository: RemoteRepository

    @Before
    fun setUp() {
        remoteRepository = RemoteRepository(RemoteDaoImpl())
    }

    @Test
    fun testGettingPrices() = runBlockingTest {
        val prices = remoteRepository.getPricesToday("testId").firstOrNull()
        Assert.assertNotEquals(prices, emptyList<PricePair>())
    }

    @Test
    fun testGettingActivity() = runBlockingTest {
        val prices = remoteRepository.getActivityTime(ActivityItem.AVERAGE_WARM_UP, "testId").firstOrNull()
        Assert.assertNotEquals(prices, emptyList<PricePair>())
    }
}