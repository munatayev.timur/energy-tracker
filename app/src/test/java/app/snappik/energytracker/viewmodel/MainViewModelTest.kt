package app.snappik.energytracker.viewmodel

import app.snappik.energytracker.database.DatabaseRepository
import app.snappik.energytracker.mocks.LocalDaoImpl
import app.snappik.energytracker.mocks.RemoteDaoImpl
import app.snappik.energytracker.model.intent.ActivityIntent
import app.snappik.energytracker.model.intent.DataIntent
import app.snappik.energytracker.model.intent.LoginIntent
import app.snappik.energytracker.model.item.ActivityItem
import app.snappik.energytracker.model.item.RegionItem
import app.snappik.energytracker.model.state.ActivityState
import app.snappik.energytracker.model.state.DataState
import app.snappik.energytracker.model.state.UserState
import app.snappik.energytracker.retrofit.RemoteRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class MainViewModelTest {

    private lateinit var viewModel: MainViewModel

    @Before
    fun setUp() {
        viewModel = MainViewModel()
        viewModel.remoteRepository = RemoteRepository(RemoteDaoImpl())
        viewModel.databaseRepository = DatabaseRepository(LocalDaoImpl())
    }

    @Test
    fun userStateTest() = runBlockingTest {
        val oldStatus = viewModel.userState.firstOrNull()
        Assert.assertEquals(oldStatus is UserState.NotLoggedIn, true)
        viewModel.loginIntent.send(LoginIntent.doLogin("itemId", RegionItem.Finland))
        val newStatus = viewModel.userState.firstOrNull()
        Assert.assertEquals(newStatus is UserState.LoggedIn, true)
        viewModel.loginIntent.send(LoginIntent.LogOut)
        val lastStatus = viewModel.userState.firstOrNull()
        Assert.assertEquals(lastStatus is UserState.NotLoggedIn, true)
    }

    @Test
    fun gettingPriceLoggedInTest() = runBlockingTest {
        val oldDataState = viewModel.dataState.firstOrNull()
        Assert.assertEquals(oldDataState is DataState.NoData, true)
        viewModel.loginIntent.send(LoginIntent.doLogin("itemId", RegionItem.Finland))
        viewModel.dataIntent.send(DataIntent.GetPrices)
        val newDataState = viewModel.dataState.firstOrNull()
        Assert.assertEquals(newDataState is DataState.TodayPrices, true)
    }

    @Test
    fun gettingPriceNotLoggedInTest() = runBlockingTest {
        val oldDataState = viewModel.dataState.firstOrNull()
        Assert.assertEquals(oldDataState is DataState.NoData, true)
        viewModel.dataIntent.send(DataIntent.GetPrices)
        val newDataState = viewModel.dataState.firstOrNull()
        Assert.assertEquals(newDataState is DataState.NoData, true)
    }


    @Test
    fun gettingActivitiesLoggedIn() = runBlockingTest {
        val oldActivitiesState = viewModel.activityState.firstOrNull()
        Assert.assertEquals(oldActivitiesState, HashMap<ActivityItem, ActivityState>())
        viewModel.loginIntent.send(LoginIntent.doLogin("itemId", RegionItem.Finland))
        viewModel.activityIntent.send(ActivityIntent.GetTimeByActivity(listOf(ActivityItem.AVERAGE_WARM_UP)))
        val newActivitiesState = viewModel.activityState.firstOrNull()
        Assert.assertNotEquals(newActivitiesState?.size ?: 0, 0)
    }

    @Test
    fun gettingActivitiesNotLoggedIn() = runBlockingTest {
        val oldActivitiesState = viewModel.activityState.firstOrNull()
        Assert.assertEquals(oldActivitiesState, HashMap<ActivityItem, ActivityState>())
        viewModel.activityIntent.send(ActivityIntent.GetTimeByActivity(listOf(ActivityItem.AVERAGE_WARM_UP)))
        val newActivitiesState = viewModel.activityState.firstOrNull()
        Assert.assertEquals(newActivitiesState?.size ?: 0, 0)
    }
}