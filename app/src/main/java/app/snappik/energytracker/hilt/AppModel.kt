package app.snappik.energytracker.hilt

import android.app.Application
import app.snappik.energytracker.R
import app.snappik.energytracker.database.LocalDao
import app.snappik.energytracker.database.SharedPrefHelper
import app.snappik.energytracker.retrofit.RemoteDao
import app.snappik.energytracker.retrofit.getHttpClient
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModel {

    @Singleton
    @Provides
    fun provideLocalDao(app: Application): LocalDao = SharedPrefHelper(app)

    @Singleton
    @Provides
    fun provideRemoteDao(application: Application): RemoteDao = Retrofit
        .Builder()
        .baseUrl(application.getString(R.string.base_url))
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .client(getHttpClient())
        .build()
        .create(RemoteDao::class.java)
}