package app.snappik.energytracker.hilt

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.snappik.energytracker.database.DatabaseRepository
import app.snappik.energytracker.retrofit.RemoteRepository
import app.snappik.energytracker.viewmodel.MainViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ViewModel {

    @Singleton
    @Provides
    fun provideMainViewModel(
        app: Application,
        databaseRepository: DatabaseRepository,
        remoteRepository: RemoteRepository
    ): MainViewModel =
        app.create(MainViewModel::class.java).apply {
            this.remoteRepository = remoteRepository
            this.databaseRepository = databaseRepository
        }

    private fun <T : ViewModel> Application.create(kk: Class<T>) = ViewModelProvider.AndroidViewModelFactory.getInstance(this).create(kk)
}