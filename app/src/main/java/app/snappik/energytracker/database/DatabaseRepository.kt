package app.snappik.energytracker.database

import app.snappik.energytracker.model.item.ActivityItem
import app.snappik.energytracker.model.item.RegionItem
import javax.inject.Inject

class DatabaseRepository @Inject constructor(private val localDao: LocalDao) {

    fun getUserId() = localDao.getUserId()

    fun setUserId(userId: String, regionItem: RegionItem) = localDao.saveUserData(userId, regionItem)

    fun getActivities() = localDao.getActivities()

    fun setActivities(list: List<ActivityItem>) = localDao.saveActivitiesData(list)

    fun clean() = localDao.clean()
}