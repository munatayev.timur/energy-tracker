package app.snappik.energytracker.database

import android.content.Context
import android.content.Context.MODE_PRIVATE
import app.snappik.energytracker.R
import app.snappik.energytracker.model.item.ActivityItem
import app.snappik.energytracker.model.item.RegionItem

class SharedPrefHelper(override val context: Context) : LocalDao {

    private val storageKey by lazy { context.getString(R.string.storage_name) }

    private val userIdKey by lazy { context.getString(R.string.user_id) }

    private val activitiesIdKey by lazy { context.getString(R.string.activities_id) }

    private val sharedPref by lazy { context.getSharedPreferences(storageKey, MODE_PRIVATE) }

    override fun saveUserData(userIdValue: String, regionItem: RegionItem) {
        val data = "$userIdValue$SEPARATOR${regionItem.name}"
        with (sharedPref.edit()) {
            putString(userIdKey, data)
            apply()
        }
    }

    override fun getUserId(): Pair<String, RegionItem>? = sharedPref.getString(userIdKey, null)?.let { data ->
        val arr = data.split(SEPARATOR)
        val userId = arr[0]
        val region = RegionItem.valueOf(arr[1])
        Pair(userId, region)
    }

    override fun saveActivitiesData(list: List<ActivityItem>) {
        val data = list.joinToString(separator = SEPARATOR) { it.name }
        with (sharedPref.edit()) {
            putString(activitiesIdKey, data)
            apply()
        }
    }

    override fun getActivities(): List<ActivityItem>{
        val data = sharedPref.getString(activitiesIdKey, null)
        return data?.split(SEPARATOR)?.map { ActivityItem.valueOf(it) } ?: emptyList()
    }

    override fun clean() = with(sharedPref.edit()){
        clear()
        commit()
    }

    companion object {
        private const val SEPARATOR = ","
    }
}