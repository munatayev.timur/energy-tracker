package app.snappik.energytracker.database

import android.content.Context
import app.snappik.energytracker.model.item.ActivityItem
import app.snappik.energytracker.model.item.RegionItem

interface LocalDao {

    val context: Context?

    fun saveUserData(userIdValue: String, regionItem: RegionItem)

    fun getUserId(): Pair<String, RegionItem>?

    fun saveActivitiesData(list: List<ActivityItem>)

    fun getActivities(): List<ActivityItem>

    fun clean(): Boolean
}