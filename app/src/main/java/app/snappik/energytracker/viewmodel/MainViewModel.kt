package app.snappik.energytracker.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.snappik.energytracker.database.DatabaseRepository
import app.snappik.energytracker.retrofit.RemoteRepository
import app.snappik.energytracker.model.intent.LoginIntent
import app.snappik.energytracker.model.state.UserState
import app.snappik.energytracker.model.intent.ActivityIntent
import app.snappik.energytracker.model.intent.DataIntent
import app.snappik.energytracker.model.item.ActivityItem
import app.snappik.energytracker.model.state.ActivityState
import app.snappik.energytracker.model.state.DataState
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {

    var remoteRepository: RemoteRepository? = null
    var databaseRepository: DatabaseRepository? = null
        set(value) {
            field = value
            viewModelScope.launch {
                field?.getUserId()?.let { (userId, region) ->
                    mUserState.emit(UserState.LoggedIn(userId, region))
                }
            }
            viewModelScope.launch {
                field?.getActivities()?.let {
                    activityIntent.send(ActivityIntent.GetTimeByActivity(it))
                }
            }
        }

    val loginIntent = Channel<LoginIntent>(UNLIMITED)
    private val mUserState = MutableStateFlow<UserState>(UserState.NotLoggedIn)
    val userState: StateFlow<UserState> = mUserState.asStateFlow()

    val dataIntent = Channel<DataIntent>(UNLIMITED)
    private val mDataState = MutableStateFlow<DataState>(DataState.NoData)
    val dataState: StateFlow<DataState> = mDataState.asStateFlow()

    val activityIntent = Channel<ActivityIntent>(UNLIMITED)
    private val mActivityState = MutableStateFlow<HashMap<ActivityItem, ActivityState>>(HashMap(emptyMap()))
    val activityState: StateFlow<HashMap<ActivityItem, ActivityState>> = mActivityState.asStateFlow()

    init {
        viewModelScope.launch {
            loginIntent.consumeAsFlow().collect { intent ->
                when(intent){
                    is LoginIntent.DoLogin -> {
                        remoteRepository?.createUser(intent.signInDto)?.collect { ok ->
                            if(ok) {
                                val userId = intent.signInDto.messengerId
                                val region = intent.regionItem
                                databaseRepository?.setUserId(userId, region)
                                mUserState.emit(UserState.LoggedIn(userId, region))
                            } else mUserState.emit(UserState.Error)
                        }
                    }
                    LoginIntent.LogOut -> {
                        databaseRepository?.clean()
                        mUserState.emit(UserState.NotLoggedIn)
                    }
                }
            }
        }
        viewModelScope.launch {
            dataIntent.consumeAsFlow().collect { dataIntent ->
                val userState = userState.value
                if(userState is UserState.LoggedIn) {
                    mDataState.emit(DataState.Loading)
                    when (dataIntent) {
                        DataIntent.GetPrices -> {
                            remoteRepository?.getPricesToday(userState.userId)?.collect {
                                mDataState.emit(DataState.TodayPrices(it))
                            }
                        }
                    }
                }
            }
        }
        viewModelScope.launch {
            activityIntent.consumeAsFlow().collect { aIntent ->
                val userState = userState.value
                if(userState is UserState.LoggedIn) {
                    when(aIntent){
                        is ActivityIntent.GetTimeByActivity -> {
                             aIntent.activityItems
                                .map { Pair(it, remoteRepository?.getActivityTime(it, userState.userId)) }
                                .map { async { it.second?.single() } }
                                .awaitAll()
                                .filterNotNull()
                                .map { it.first to ActivityState.ActivityPrices(it.second) }
                                .toMap()
                                .run { mActivityState.emit(HashMap(this)) }
                        }
                        is ActivityIntent.SetTimeByActivities -> {
                            databaseRepository?.setActivities(aIntent.activityItems)
                            activityIntent.send(ActivityIntent.GetTimeByActivity(aIntent.activityItems))
                        }
                    }
                }
            }
        }
    }
}