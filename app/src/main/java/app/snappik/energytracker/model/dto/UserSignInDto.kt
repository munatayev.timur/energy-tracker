package app.snappik.energytracker.model.dto

import java.util.*

data class UserSignInDto(
    val electricityMarketZone: String,
    val messengerId: String = UUID.randomUUID().toString(),
    val language: String = "en",
    val phoneNumber: String = UUID.randomUUID().toString(),
    val firstName: String = ANONYMOUS,
    val lastName: String = ANONYMOUS,
    val subscription: Boolean = false
) {
    companion object {
        private const val ANONYMOUS = "Android anonymous user"
    }
}