package app.snappik.energytracker.model.state

import android.content.Context
import app.snappik.energytracker.R
import app.snappik.energytracker.model.PricePair
import app.snappik.energytracker.utils.currentHour
import kotlin.math.abs
import kotlin.math.round

sealed class DataState{
    object NoData : DataState(){
        override fun getSubString(context: Context): Pair<String, Boolean> = Pair(context.getString(
            R.string.data_no_data), false)
        override fun toString(context: Context): String = context.getString(
            R.string.data_no_data)
    }
    object Loading : DataState(){
        override fun getSubString(context: Context): Pair<String, Boolean> = Pair(context.getString(
            R.string.data_no_data), false)
        override fun toString(context: Context): String = context.getString(
            R.string.data_state_loading)
    }
    data class TodayPrices(val prices: List<PricePair>): DataState(){
        override fun toString(context: Context): String {
            val currentHour = currentHour()
            val currentPrice = prices.firstOrNull { it.hour == currentHour }
            return currentPrice?.let { "${it.price}" } ?: context.getString(
                R.string.data_state_no_prices)
        }
        override fun getSubString(context: Context): Pair<String, Boolean> {
            val currentHour = currentHour()
            val currentPrice = prices.firstOrNull { it.hour == currentHour }?.price ?: 0.0
            val middle = prices.sumOf { it.price } / prices.size
            val dif = currentPrice - middle
            val percent = round(abs(100 * dif / middle)).toInt()
            val text = if(dif > 0) String.format(context.getString(
                R.string.data_state_higher), percent) else String.format(context.getString(
                R.string.data_state_lower), percent)
            val colorChange = dif < 0
            return Pair(text, colorChange)
        }
    }

    abstract fun toString(context: Context): String
    abstract fun getSubString(context: Context): Pair<String, Boolean>
}
