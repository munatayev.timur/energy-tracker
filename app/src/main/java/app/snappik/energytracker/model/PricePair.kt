package app.snappik.energytracker.model

data class PricePair(val hour: Int, val price: Double)