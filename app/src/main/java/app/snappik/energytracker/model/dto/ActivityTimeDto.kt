package app.snappik.energytracker.model.dto

data class ActivityTimeDto(
    val morning: Map<Int, PartDayResult>,
    val afternoon: Map<Int, PartDayResult>,
    val evening: Map<Int, PartDayResult>
) {
    data class PartDayResult(
        val savings: Double,
        val hour: Int
    )
}