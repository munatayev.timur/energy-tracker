package app.snappik.energytracker.model.intent

import app.snappik.energytracker.model.dto.UserSignInDto
import app.snappik.energytracker.model.item.RegionItem

sealed class LoginIntent {
    data class DoLogin(val signInDto: UserSignInDto, val regionItem: RegionItem) : LoginIntent()
    object LogOut: LoginIntent()

    companion object {
        fun doLogin(itemId: String, item: RegionItem) = DoLogin(UserSignInDto(itemId), item)
    }
}