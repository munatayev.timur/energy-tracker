package app.snappik.energytracker.model.intent

import app.snappik.energytracker.model.item.ActivityItem

sealed class ActivityIntent{
    data class GetTimeByActivity(val activityItems: List<ActivityItem>) : ActivityIntent()
    data class SetTimeByActivities(val activityItems: List<ActivityItem>) : ActivityIntent()
}
