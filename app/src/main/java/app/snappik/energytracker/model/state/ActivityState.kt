package app.snappik.energytracker.model.state

import android.content.Context
import app.snappik.energytracker.R
import app.snappik.energytracker.model.PricePair

sealed class ActivityState{
    object Loading : ActivityState(){
        override fun toString(context: Context): String = context.getString(
            R.string.activity_state_loading)
    }
    data class ActivityPrices(val price: PricePair?): ActivityState() {
        override fun toString(context: Context): String {
            return price?.let { String.format(context.getString(R.string.activity_state_start_at), price.hour) }
                ?: context.getString(R.string.activity_state_start_now)
        }
    }
    abstract fun toString(context: Context): String
}
