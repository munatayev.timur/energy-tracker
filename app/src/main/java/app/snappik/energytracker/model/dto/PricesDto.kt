package app.snappik.energytracker.model.dto

import app.snappik.energytracker.model.PricePair

data class PricesDto(
    val morning: List<PricePair>,
    val afternoon: List<PricePair>,
    val evening: List<PricePair>
)