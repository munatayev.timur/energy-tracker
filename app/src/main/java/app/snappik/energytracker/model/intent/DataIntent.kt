package app.snappik.energytracker.model.intent

sealed class DataIntent{
    object GetPrices: DataIntent()
}
