package app.snappik.energytracker.model.state

import android.content.Context
import app.snappik.energytracker.R
import app.snappik.energytracker.model.item.RegionItem

sealed class UserState {
    object NotLoggedIn : UserState(){
        override fun toString(context: Context): String {
            return context.getString(R.string.user_state_not_logged_in)
        }
    }
    data class LoggedIn(val userId: String, val region: RegionItem) : UserState(){
        override fun toString(context: Context): String {
            return context.getString(region.resName)
        }
    }
    object Error : UserState(){
        override fun toString(context: Context): String {
            return context.getString(R.string.user_state_error)
        }
    }

    abstract fun toString(context: Context): String
}