package app.snappik.energytracker.model.item

import app.snappik.energytracker.R

enum class RegionItem(val resId: Int, val resName: Int) {
    Finland(R.string.region_finland, R.string.region_finland_name),
    Norway_Oslo(R.string.region_norway1, R.string.region_norway1_name),
    Norway_Kristiansand(R.string.region_norway2, R.string.region_norway2_name),
    Norway_Trondheim(R.string.region_norway3, R.string.region_norway3_name),
    Norway_Tromso(R.string.region_norway4, R.string.region_norway4_name),
    Norway_Bergen(R.string.region_norway5, R.string.region_norway5_name),
    Denmark_Vest(R.string.region_denmark1, R.string.region_denmark1_name),
    Denmark_Ost(R.string.region_denmark2, R.string.region_denmark2_name),
    Estonia(R.string.region_estonia, R.string.region_estonia_name)
}