package app.snappik.energytracker.model.dto

enum class Which { CHEAPEST, COST_MOST }