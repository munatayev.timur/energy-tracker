package app.snappik.energytracker.model.dto

enum class When { TODAY, TOMORROW }