package app.snappik.energytracker.model.item

import app.snappik.energytracker.R

enum class ActivityItem(val res: Int) {
    TAKE_A_SHOWER(R.string.activity_take_shower),
    COOK_FOOD(R.string.activity_cook_food),
    COOK_FOOD_AND_DISHWASHER(R.string.activity_cook_food_and_dishwasher),
    DO_LAUNDRY(R.string.activity_do_laundry),
    DO_LAUNDRY_DRYER(R.string.activity_do_laundry_dryer),
    CHARGE_SMALL_CAR(R.string.activity_charge_small_car),
    CHARGE_LARGE_CAR(R.string.activity_charge_large_car),
    VACUUM_YOUR_PLACE(R.string.activity_vacuum_your_place),
    MILD_WARM_UP(R.string.activity_mild_warm_up),
    AVERAGE_WARM_UP(R.string.activity_average_warm_up),
    INTENSE_WARM_UP(R.string.activity_intense_warm_up),
    ADD_FAVORITE(R.string.activity_add);

    companion object{
        fun getDefault() = mapOf(ADD_FAVORITE to "+")
    }
}