package app.snappik.energytracker.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.snappik.energytracker.R
import app.snappik.energytracker.databinding.FragmentMainBinding
import app.snappik.energytracker.model.intent.ActivityIntent
import app.snappik.energytracker.model.intent.DataIntent
import app.snappik.energytracker.model.intent.LoginIntent
import app.snappik.energytracker.model.item.ActivityItem
import app.snappik.energytracker.model.state.ActivityState
import app.snappik.energytracker.model.state.DataState
import app.snappik.energytracker.model.state.UserState
import app.snappik.energytracker.ui.dialog.showSingleChoseDialog
import app.snappik.energytracker.ui.splash.SplashActivity
import app.snappik.energytracker.utils.currentHour
import app.snappik.energytracker.viewmodel.MainViewModel
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class MainFragment : AbstractFragment<FragmentMainBinding>() {

    @Inject lateinit var mainViewModel: MainViewModel

    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentMainBinding.inflate(inflater, container, false)
        .apply { viewModel = mainViewModel }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            mainViewModel.dataIntent.send(DataIntent.GetPrices)
            mainViewModel.userState.collect {
                if(it is UserState.NotLoggedIn) openActivity(SplashActivity::class.java)
            }
        }
    }
}

@BindingAdapter("data")
fun data(barChart: BarChart, data: DataState) {
    when(data){
        is DataState.TodayPrices -> {
            barChart.visibility = View.VISIBLE
            val values = data.prices.map { BarEntry(it.hour.toFloat(), it.price.toFloat()) }
            val barDataSet = BarDataSet(values, "")
            val currentHour = currentHour()
            val inactiveBarColor = ContextCompat.getColor(barChart.context, R.color.chart_bar)
            val activeBarColor = ContextCompat.getColor(barChart.context, R.color.chart_bar_active)
            barDataSet.colors = values.map { if(it.x == currentHour.toFloat()) activeBarColor else inactiveBarColor }
            barDataSet.isHighlightEnabled = false
            barChart.data = BarData(barDataSet).apply { setDrawValues(false) }
            barChart.description = Description().apply { isEnabled = false }
            barChart.xAxis.apply {
                position = XAxis.XAxisPosition.BOTTOM
                textSize = 12f
                setDrawAxisLine(false)
                setDrawGridLines(false)
            }
            barChart.axisLeft.apply { setDrawAxisLine(false) }
            barChart.axisRight.apply {
                setDrawLabels(false)
                setDrawAxisLine(false)
            }
            barChart.legend.apply { form = Legend.LegendForm.EMPTY }
            barChart.setDrawBorders(false)
            barChart.animateY(1000)
            barChart.invalidate()

        }
        else -> barChart.visibility = View.GONE
    }
}

@BindingAdapter(value= ["activities", "viewModel"], requireAll = false)
fun activities(recyclerView: RecyclerView, activities: HashMap<ActivityItem, ActivityState>, viewModel: MainViewModel) {
    val list = activities.mapValues { it.value.toString(recyclerView.context) }
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = ActivityAdapter(if(list.isEmpty()) ActivityItem.getDefault() else list) {
        recyclerView.context.showSingleChoseDialog(activities.keys.toList()){
            viewModel.viewModelScope.launch {
                viewModel.activityIntent.send(ActivityIntent.SetTimeByActivities(it))
            }
        }
    }
}

@BindingAdapter("logout")
fun logout(imageView: ImageView, viewModel: MainViewModel) {
    imageView.setOnClickListener {
        viewModel.viewModelScope.launch {
            viewModel.loginIntent.send(LoginIntent.LogOut)
        }
    }
}

@BindingAdapter("average")
fun average(textView: TextView, subString: Pair<String, Boolean>) {
    textView.text = subString.first
    textView.setTextColor(ContextCompat.getColor(textView.context,
        if(subString.second) R.color.average_green else R.color.average_red ))
}