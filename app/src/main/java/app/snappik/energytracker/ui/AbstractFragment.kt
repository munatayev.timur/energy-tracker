package app.snappik.energytracker.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class AbstractFragment<T : ViewDataBinding> : Fragment() {

    protected var viewDataBinding: T? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = (viewDataBinding ?: getBinding(layoutInflater, container, savedInstanceState)
        .also { it.init(savedInstanceState) })
        .apply {
            lifecycleOwner = this@AbstractFragment.viewLifecycleOwner
            viewDataBinding = this@apply
        }
        .run { root }

    protected abstract fun getBinding(inflater: LayoutInflater,
                                      container: ViewGroup?,
                                      savedInstanceState: Bundle?): T

    protected open fun T.saveState(outState: Bundle) = Unit

    protected open fun T.init(state: Bundle?) = Unit

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewDataBinding?.saveState(outState)
    }

    protected fun openActivity(openClass: Class<*>){
        Intent(requireContext(), openClass)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .also { activity?.startActivity(it) }
    }

    protected inline fun <reified T> getParentActivity(): T? = if(activity is T) (activity as T) else null
}