package app.snappik.energytracker.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.snappik.energytracker.databinding.FragmentLoginBinding
import app.snappik.energytracker.model.intent.ActivityIntent
import app.snappik.energytracker.model.intent.LoginIntent
import app.snappik.energytracker.model.item.ActivityItem
import app.snappik.energytracker.model.item.RegionItem
import app.snappik.energytracker.model.state.ActivityState
import app.snappik.energytracker.model.state.UserState
import app.snappik.energytracker.ui.AbstractFragment
import app.snappik.energytracker.ui.ActivityAdapter
import app.snappik.energytracker.ui.MainActivity
import app.snappik.energytracker.ui.dialog.showSingleChoseDialog
import app.snappik.energytracker.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : AbstractFragment<FragmentLoginBinding>() {

    @Inject lateinit var mainViewModel: MainViewModel

    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentLoginBinding.inflate(inflater, container, false)
        .apply { viewModel = mainViewModel }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            mainViewModel.userState.collect {
                if(it is UserState.LoggedIn){
                    openActivity(MainActivity::class.java)
                }
            }
        }
    }
}

@BindingAdapter("regions")
fun regions(recyclerView: RecyclerView, viewModel: MainViewModel) {
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = RegionAdapter { item ->
        viewModel.viewModelScope.launch {
            val itemId = recyclerView.context.getString(item.resId)
            viewModel.loginIntent.send(LoginIntent.doLogin(itemId, item))
        }
    }
}