package app.snappik.energytracker.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.snappik.energytracker.databinding.ItemActivityBinding
import app.snappik.energytracker.model.item.ActivityItem

class ActivityAdapter(
    private val items: Map<ActivityItem, String> = emptyMap(),
    private val onItemClicked: () -> Unit
) : RecyclerView.Adapter<ActivityAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ItemActivityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            .run { ViewHolder(this, onItemClicked)
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items.entries.toList()[position])

    class ViewHolder(
        private val viewDataBinding: ItemActivityBinding,
        private val onItemClicked: () -> Unit
    ) : RecyclerView.ViewHolder(viewDataBinding.root){
        fun bind(item: Map.Entry<ActivityItem, String>) {
            viewDataBinding.name = viewDataBinding.root.context.getString(item.key.res)
            viewDataBinding.time = item.value
            viewDataBinding.root.setOnClickListener { onItemClicked.invoke() }
        }
    }
}