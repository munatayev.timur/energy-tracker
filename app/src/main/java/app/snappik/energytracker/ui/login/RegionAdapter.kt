package app.snappik.energytracker.ui.login

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.snappik.energytracker.databinding.ItemRegionBinding
import app.snappik.energytracker.model.item.RegionItem

class RegionAdapter(
    private val onItemClicked: (RegionItem) -> Unit
) : RecyclerView.Adapter<RegionAdapter.ViewHolder>() {

    private val items: List<RegionItem> = RegionItem.values().toList()

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ItemRegionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            .run { ViewHolder(this, onItemClicked)
            }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position])

    class ViewHolder(
        private val viewDataBinding: ItemRegionBinding,
        private val onItemClicked: (RegionItem) -> Unit
    ) : RecyclerView.ViewHolder(viewDataBinding.root){
        fun bind(item: RegionItem) {
            viewDataBinding.name = viewDataBinding.root.context.getString(item.resName)
            viewDataBinding.root.setOnClickListener { onItemClicked.invoke(item) }
        }
    }
}