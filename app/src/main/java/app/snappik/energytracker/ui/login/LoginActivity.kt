package app.snappik.energytracker.ui.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.snappik.energytracker.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}