package app.snappik.energytracker.ui.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import app.snappik.energytracker.R
import app.snappik.energytracker.ui.MainActivity
import app.snappik.energytracker.ui.login.LoginActivity
import app.snappik.energytracker.model.state.UserState
import app.snappik.energytracker.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    @Inject lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        lifecycleScope.launch {
            mainViewModel.userState.collect { state ->
                if(state is UserState.LoggedIn) openNext(MainActivity::class.java)
            }
        }
        findViewById<Button>(R.id.button).setOnClickListener {
            openNext(LoginActivity::class.java)
        }
    }

    private fun openNext(openClass: Class<*>){
        Intent(this, openClass)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .also { startActivity(it) }
    }
}