package app.snappik.energytracker.ui.dialog

import android.app.AlertDialog
import android.content.Context
import app.snappik.energytracker.R
import app.snappik.energytracker.model.item.ActivityItem

fun Context.showSingleChoseDialog(
    selected: List<ActivityItem> = emptyList(),
    onItemsSelected: (List<ActivityItem>) -> Unit,
){
    val all = ActivityItem.values().filter { it != ActivityItem.ADD_FAVORITE }.map { getString(it.res) }.toTypedArray()
    val selectedItem = all.map { items -> selected.map { getString(it.res) }.contains(items) }.toBooleanArray()

    val newSelected = ArrayList<ActivityItem>(selected)

    AlertDialog.Builder(this, R.style.AlertDialogStyle)
        .setCancelable(true)
        .setTitle(getString(R.string.select_activities))
        .setMultiChoiceItems(all, selectedItem
        ) { _, which, isChecked ->
            if(isChecked){
                val item = ActivityItem.values().first { getString(it.res) == all[which] }
                newSelected.add(item)
            } else {
                val item = ActivityItem.values().first { getString(it.res) == all[which] }
                newSelected.indexOf(item).let {
                    newSelected.removeAt(it)
                }
            }
        }
        .setPositiveButton(R.string.general_ok) { _, _ ->
            onItemsSelected.invoke(newSelected)
        }
        .show()
}