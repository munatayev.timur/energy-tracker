package app.snappik.energytracker.utils

import java.text.SimpleDateFormat
import java.util.*


fun currentHour(): Int {
    val sdf = SimpleDateFormat("HH", Locale.ENGLISH)
    val now = System.currentTimeMillis()
    val date = Date(now)
    return sdf.format(date).toInt()
}

fun currentDate(): String {
    val sdf = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
    val now = System.currentTimeMillis()
    val date = Date(now)
    return sdf.format(date)
}