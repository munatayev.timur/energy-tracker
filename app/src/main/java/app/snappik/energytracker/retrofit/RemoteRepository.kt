package app.snappik.energytracker.retrofit

import app.snappik.energytracker.model.PricePair
import app.snappik.energytracker.model.dto.PricesDto
import app.snappik.energytracker.model.dto.UserSignInDto
import app.snappik.energytracker.model.dto.When
import app.snappik.energytracker.model.dto.Which
import app.snappik.energytracker.model.item.ActivityItem
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class RemoteRepository @Inject constructor(private val remoteDao: RemoteDao) {

    suspend fun getPricesToday(messengerId: String) = flow {
        remoteDao.getPrices(
            Which.CHEAPEST.toString().lowercase(),
            When.TODAY.toString().lowercase(),
            messengerId
        ).also { emit(it) }
    }.transform<PricesDto, List<PricePair>> { value ->
        ArrayList<PricePair>().apply {
            addAll(value.morning)
            addAll(value.afternoon)
            addAll(value.evening)
            sortBy { it.hour }
        }.also { emit(it) }
    }.catch {
        emit(emptyList())
    }

    suspend fun getActivityTime(activity: ActivityItem, messengerId: String) = flow {
        remoteDao.getActivityTime(
            activity.ordinal.toString(),
            When.TODAY.toString().lowercase(),
            messengerId
        ).also { emit(it) }
    }.transform { value ->
        ArrayList<PricePair>().apply {
            addAll(value.morning.map { PricePair(it.value.hour, it.value.savings) })
            addAll(value.afternoon.map { PricePair(it.value.hour, it.value.savings) })
            addAll(value.evening.map { PricePair(it.value.hour, it.value.savings) })
            sortBy { it.price }
        }.firstOrNull { it.hour > 0 }.also { emit(Pair(activity, it)) }
    }.catch {
        emit(Pair(activity, null))
    }

    suspend fun createUser(userSignInDto: UserSignInDto) = flow {
        remoteDao.createUser(userSignInDto)
        emit(true)
    }.catch {
        emit(false)
    }
}