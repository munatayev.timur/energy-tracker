package app.snappik.energytracker.retrofit

import app.snappik.energytracker.model.dto.ActivityTimeDto
import app.snappik.energytracker.model.dto.PricesDto
import app.snappik.energytracker.model.dto.UserSignInDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RemoteDao {

    @GET("/activity/prices")
    suspend fun getPrices(@Query("which") which: String,
                          @Query("when") `when`: String,
                          @Query("messengerId") messengerId: String): PricesDto

    @GET("/activity")
    suspend fun getActivityTime(@Query("what") what: String,
                          @Query("when") `when`: String,
                          @Query("messengerId") messengerId: String): ActivityTimeDto

    @POST("/user")
    suspend fun createUser(@Body userSignInDto: UserSignInDto)
}