package app.snappik.energytracker.retrofit

import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

private const val REQUEST_TIMEOUT = 10L
private const val REQUEST_MAX = 1

fun getHttpClient() = OkHttpClient.Builder()
    .apply {
        followRedirects(false)
        followSslRedirects(false)
        dispatcher(Dispatcher().apply { maxRequests = REQUEST_MAX })
        readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
    }.build()